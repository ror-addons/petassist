by SilverWF

Lightweight and simple addon to handle your pet - that was my main goal while creating this.
Unmatter of pet's stance, addon would force your pet to attack your current target.

Commands:
/pa on - turn on addon
/pa off - turn it off

You can also create an ingame script with text:
/script PetAssist.ChangeState ()
and put it on the hotbar. Pressing on it would toggle assisting ON or OFF.

What is new:

ver 5 - mar 2020
Now pet got some kind of smart logic (in this priority):
1. if master use detaunt, Pet heal (WL), Run away (SH) or calling mount - pet would return to you
2. if master use any damaging or debuffing skill - then pet would attack your target
3. if master use any another skill - pet would continue do his previous action
* turrets and daemons has no option to return to you, so point 1 didn't affect them

ver 4 - nov 2019
Added script for toggle macro.

ver 3 - jan 2018
Added pet's skills handler. Now your pet would try to use all his available skills by turn, starting from right to left direction on pet's skills bar. No more delays: if skill not on cooldown - it would be used immediately!
For more smarter skills usage I am still recommend to use LoayalPet addon tho. 

ver 2 - dec 2017
Pet would attack at every skill (before only on hostile), exclude Summon Mount skills - this would force your pet to follow you.

ver 1 - long long time ago at live servers...
Initial release