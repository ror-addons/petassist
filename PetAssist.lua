PetAssist = {}
PetAssist.enabled = true
local pairs = pairs
local towstring = towstring
local Print = EA_ChatWindow.Print

local function CheckCareer()  -- 102 - WL, 27 - SH, 67 - Mag, 23 - Eng
	if GameData.Player.career.id == 102 or GameData.Player.career.id == 27 or GameData.Player.career.id == 67 or GameData.Player.career.id == 23 then
		return true
	else
		return false
	end
end

function PetAssist.Initialize()
	if not CheckCareer() then
		Print(L"PetAssist is not loaded - your career do not have pets.")
		return
	end 

	LibSlash.RegisterSlashCmd("pa", function(input) PetAssist.SlashHandler(input) end)
	RegisterEventHandler("PetAssist", SystemData.Events.PLAYER_BEGIN_CAST, "PetAssist.OnPlayerCast")
	Print(L"PetAssist loaded. To switch modes print '/pa on' or '/pa off'")

	if PetAssist.enabled then
		Print(L"PetAssist is ON. Your pet will attack your target at the every hostile spell use.")
	else
		Print(L"PetAssist is OFF. Your pet will follow ingame pet stance rules.")
	end 
end

function PetAssist.Shutdown()
	UnRegisterEventHandler("PetAssist", SystemData.Events.PLAYER_BEGIN_CAST, "PetAssist.OnPlayerCast")    
end

local function IsOffensive (abilityId)
	local abilityData = {}
	abilityData = GetAbilityData(abilityId)
	if abilityData.isDamaging or abilityData.isDebuff or abilityData.isOffensive then -- isDefensive, isCurse, isHex, isAilment, isBuff
		return true
		else
		return false
	end
end

local function IsDefensive (abilityId)
-- 9164 WL pet heal, 9169 WL detaunt, 1827 SH detaunt, 1852 SH Run away!, 14593-15614 Mounts
	if abilityId == 9164 or abilityId == 9169 or abilityId == 1827 or abilityId == 1852 or (abilityId >= 14593 and abilityId <= 15614) then 
		return true
		else
		return false
	end
end

function PetAssist.OnPlayerCast(abilityId, isChannel, desiredCastTime, averageLatency)
	if PetAssist.enabled and PetWindow.HasPet() then 
		if IsDefensive (abilityId) then
			CommandPet(GameData.PetCommand.FOLLOW)
		elseif IsOffensive (abilityId) then
			PetAssist.PetAttack()
		else
			return
		end
	else
		return
	end
end

function PetAssist.PetAttack()
	CommandPet(GameData.PetCommand.ATTACK)
	local abilityTable = GetAbilityTable(GameData.AbilityType.PET)
	for key, val in pairs (abilityTable) do 
		CommandPetDoAbility(key)
	end
end

function PetAssist.SlashHandler(args)
	local opt, val = args:match("(%w+)[ ]?(.*)")
	if opt == "on" then
		PetAssist.enabled = true
		Print(L"PetAssist is ON. Your pet will attack your target at the every hostile spell use.")
	elseif opt == "off" then
		PetAssist.enabled = false
		Print(L"PetAssist is OFF. Your pet will follow ingame pet stance rules now.")
	else
		Print(L"To switch modes print '/pa on' or '/pa off' ")
	end
end

function PetAssist.ChangeState ()
	if PetAssist.enabled == true then
		PetAssist.enabled = false
		Print(L"PetAssist is OFF. Your pet will follow ingame pet stance rules now.")
	else PetAssist.enabled = true
		Print(L"PetAssist is ON. Your pet will attack your target at the every hostile spell use.")
	end
end

-- Icon show purposes, create macro with text '/script PetAssist.ChatTest ()' to work
local iconNum = 0

function PetAssist.ChatTest ()
	Print (L"Icons "..towstring(iconNum)..L"-"..towstring(iconNum+9)..L" : "..L"  <icon"..towstring(iconNum)..L">"..L"  <icon"..towstring(iconNum+1)..L">"..L"  <icon"..towstring(iconNum+2)..L">"..L"  <icon"..towstring(iconNum+3)..L">"..L"  <icon"..towstring(iconNum+4)..L">"..L"  <icon"..towstring(iconNum+5)..L">"..L"  <icon"..towstring(iconNum+6)..L">"..L"  <icon"..towstring(iconNum+7)..L">"..L"  <icon"..towstring(iconNum+8)..L">"..L"  <icon"..towstring(iconNum+9)..L">")
	iconNum = iconNum +10
end