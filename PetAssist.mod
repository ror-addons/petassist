<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <UiMod name="PetAssist" version="5.0" date="25/03/2020" >
    <Author name="SilverWF" email="silverwf@gmail.com" />
    <Description text="Forces Pet to Attack your target at every spell cast. This addon didn't have any settings: just put your pet into the passive mode and enjoy. Also read Readme file!" />
    <VersionSettings gameVersion="1.4.8" />
    <Dependencies>
      <Dependency name="LibSlash" />
    </Dependencies>
    <Files>
      <File name="PetAssist.lua" />
    </Files>
    <OnInitialize>
      <CallFunction name="PetAssist.Initialize" />
    </OnInitialize>
    <WARInfo>
      <Careers>
        <Career name="ENGINEER" />
        <Career name="SQUIG_HERDER" />
        <Career name="MAGUS" />
        <Career name="WHITE_LION" />
      </Careers>
    </WARInfo>
  </UiMod>
</ModuleFile>